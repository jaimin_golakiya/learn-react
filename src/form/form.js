import React, { Fragment } from "react";
import Userlist from "./userlist";


class Form extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            form: {
                id: 'Testform',
                fields: {name : null},
            },
            error: {},
        }
    }


    formSubmit(e) {
        e.preventDefault();
        if (this.handleValidation()) {
            let submited_arr = [];
            if(localStorage.getItem('users')){
                submited_arr = JSON.parse(localStorage.getItem('users'));
            }
            submited_arr.push(this.state.form.fields);
            localStorage.setItem('users',JSON.stringify(submited_arr));
        }
    }

    handleValidation() {
        let user_field = this.state.form.fields;
        let error = {};
        if (!user_field['name']) {
            error['name'] = 'please enter name';
        }
        if (!user_field['email']) {
            error['email'] = 'please enter email';
        } else if (!new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(user_field['email'])) {
            error['email'] = 'please enter valid email';
        }

        if (!user_field['gender']) {
            error['gender'] = 'please select gender';
        }
        if (!user_field['password']) {
            error['password'] = 'please enter password';
        }
        if (!user_field['confirm_password']) {
            error['confirm_password'] = 'please enter confirm password';
        } else if (user_field['confirm_password'] !== user_field['password']) {
            error['confirm_password'] = 'please are not same';
        }

        this.setState({ error: error });
        console.log(error);
        return (error.length > 0) ? true : false;
    }

    handleChange(field, e) {
        let form = this.state.form;
        let fields = form.fields;
        fields[field] = e.target.value;
        form.fields = fields;
        this.setState({ form : form });
        this.handleValidation();
    }


    render() {
        return (
            <Fragment>
                <div>
                    <form name="contactform" className="contactform"   onSubmit={this.formSubmit.bind(this)}>
                        <fieldset>
                            <label> Name: </label>
                            <input type="text" refs="name" name="name" placeholder="Please enter name" onChange={this.handleChange.bind(this, "name")}/>
                            <span style={{color: "red"}}>{this.state.error["name"]}</span>
                        </fieldset>
                        <fieldset>
                            <label> Email: </label>
                            <input type="email" refs="email" name="email" placeholder="Please enter email" onChange={this.handleChange.bind(this, "email")}/>
                            <span style={{color: "red"}}>{this.state.error["email"]}</span>
                        </fieldset>
                        <fieldset>
                            <label> Gender: </label>
                            <div>
                                <label>Male</label>
                                <input type="radio" refs="gender" name="gender" value="male" onChange={this.handleChange.bind(this, "gender")}/>
                            </div>
                            <div>
                                <label>Female</label>
                                <input type="radio" refs="gender" name="gender" value="female" onChange={this.handleChange.bind(this, "gender")}/>
                            </div>
                            <span style={{color: "red"}}>{this.state.error["gender"]}</span>
                        </fieldset>
                        <fieldset>
                            <label> Password: </label>
                            <input type="password" refs="password" name="password" placeholder="Please enter password" onChange={this.handleChange.bind(this, "password")}/>
                            <span style={{color: "red"}}>{this.state.error["password"]}</span>
                        </fieldset>
                        <fieldset>
                            <label> Confirm Password: </label>
                            <input type="password" refs="confirm_password" name="confirm_password" placeholder="Please enter confirm password" onChange={this.handleChange.bind(this, "confirm_password")}/>
                            <span style={{color: "red"}}>{this.state.error["confirm_password"]}</span>
                        </fieldset>
                        <input type="submit" value="Submit" />
                    </form>
                </div>
                <div>
                <Userlist />
                </div>
            </Fragment>
        )
    }
}


export default Form;
