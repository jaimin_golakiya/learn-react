import React, { useState, Fragment, useEffect } from "react";

function Userlist(props) {

    const [users, setusers] = useState(true);

    useEffect((props) => {
        let Checkuser = localStorage.getItem('users');
        if (Checkuser) {
            setusers(localStorage.getItem('users'));
        }
    })


    function deleteuser(index) {
        console.log(index);
        if (users) {
            console.log(index);
            let remove_index = JSON.parse(users);
            remove_index = remove_index.filter(function(item,arr_index) {
                return arr_index !== index
            })
            console.log(remove_index)
            localStorage.setItem('users', JSON.stringify(remove_index));
            setusers(JSON.stringify(remove_index));
        }
    }



    return (
        <Fragment>

            <h3>User List</h3>

            {users.length ? (
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {JSON.parse(users).map((data, index) => (
                            <tr key={index}>
                                <td>{data.name}</td>
                                <td>{data.email}</td>
                                <td>{data.gender}</td>
                                <td>
                                    <button type="button" onClick={(e) => {
                                        deleteuser(index);
                                    }}>Delete</button>
                                </td>
                            </tr>
                        ))}

                    </tbody>
                </table>
            ) : (
                <div>No User Founded</div>

            )}
        </Fragment>
    )

}

export default Userlist;